FROM node:lts as builder

ENV NODE_OPTIONS=--openssl-legacy-provider

WORKDIR /app

COPY . .

RUN yarn install --immutable

RUN yarn build

FROM nginx:alpine

WORKDIR /app

COPY --from=builder /app/dist  /usr/share/nginx/html

ENV HOST 0.0.0.0
EXPOSE 80
EXPOSE 443