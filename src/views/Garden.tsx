import { GardenDashboard } from '../garden/GardenDashboard';

const Garden = () => {
  return <GardenDashboard />;
};

/**
 * Needed for React.lazy
 */
// eslint-disable-next-line import/no-default-export
export default Garden;
