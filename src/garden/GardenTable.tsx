import { styled } from '@mui/material/styles';
import { DataGrid } from '@mui/x-data-grid/DataGrid';
import type { GridColDef, GridRowParams } from '@mui/x-data-grid/models';
import { FC, useCallback, useMemo } from 'react';

import { useLanguage } from '../resources/lang/LanguageContext';
import type { ILanguageItems } from '../resources/lang/type';

import type { IDataSourceItem } from './types';

export interface IGardenProps {
  dataSource: Partial<Record<keyof ILanguageItems, IDataSourceItem>>;
  highlightItem: keyof ILanguageItems;
  labelRatio: string;
  sourceItems: Partial<Record<keyof ILanguageItems, number>>;
}

const StyledDataGrid = styled(DataGrid)(({ theme }) => ({
  '& .highlight': {
    '&:hover': {
      backgroundColor: theme.palette.action.selected
    },
    backgroundColor: theme.palette.action.hover
  }
}));

export const GardenTable: FC<IGardenProps> = ({ dataSource, highlightItem, labelRatio, sourceItems }) => {
  const lang = useLanguage();

  const rows = useMemo(() => {
    const preparedData: { id: string; name: string; price: number; ratio: number; value: number; highlight: boolean }[] = [];

    Object.keys(dataSource).forEach((key) => {
      const price = dataSource[key as keyof typeof dataSource];
      const name = lang.items[key as keyof ILanguageItems];

      preparedData.push({
        highlight: key === highlightItem,
        id: key,
        name,
        price: price?.price ?? 0,
        ratio: price?.ratio ?? 1,
        value: sourceItems[key as keyof typeof sourceItems] ?? 0
      });
    });

    preparedData.filter((item) => item.price !== 0 && item.value !== 0).sort((a, b) => a.name.localeCompare(b.name));

    return preparedData;
  }, [dataSource, highlightItem, lang.items, sourceItems]);

  const columns = useMemo(() => {
    const columnsMemo: GridColDef[] = [
      { field: 'name', flex: 5, headerName: lang.ui.item, sortable: true },
      {
        align: 'right',
        field: 'price',
        flex: 3,
        headerName: lang.ui.itemPrice,
        sortable: true,
        valueFormatter: ({ value }) => {
          return Math.ceil(value).toLocaleString();
        }
      },
      {
        align: 'right',
        field: 'ratio',
        flex: 3,
        headerName: lang.ui.itemPricePerCompost,
        sortable: true,
        valueFormatter: ({ value }) => {
          return Math.ceil(value).toLocaleString();
        }
      },
      {
        align: 'right',
        field: 'value',
        flex: 4,
        headerName: labelRatio,
        sortable: true,
        valueFormatter: ({ value }) => {
          return Math.ceil(value).toLocaleString();
        }
      }
    ];

    return columnsMemo;
  }, [labelRatio, lang.ui.item, lang.ui.itemPrice, lang.ui.itemPricePerCompost]);

  const getRowClassName = useCallback((params: GridRowParams) => {
    return params.row.highlight ? 'highlight' : '';
  }, []);

  return <StyledDataGrid columns={columns} getRowClassName={getRowClassName} rows={rows} />;
};
