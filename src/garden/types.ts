export interface IDataSourceItem {
  price: number;
  ratio: number;
}
