import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import type { FC } from 'react';

import { itemsFuels, itemsOrganicMatter } from '../resources/garden';
import { useLanguage } from '../resources/lang/LanguageContext';

import { Compost } from './Compost';
import { GardenChart } from './GardenChart';
import { GardenTable } from './GardenTable';
import { useGardenPrice } from './functions';

export const GardenDashboard: FC = () => {
  const { ui } = useLanguage();

  const { compost, fuels, organicMatters } = useGardenPrice();

  return (
    <Box component="main" display="grid" gap={2} gridTemplateColumns="repeat(12, 1fr)" sx={{ width: '100%' }}>
      <Box gridColumn="span 12">
        <Compost compost={compost} fuels={fuels} organicMatters={organicMatters} />
      </Box>

      <Box gridColumn="span 12">
        <Typography variant="h3">{ui.organicMattersTitle}</Typography>
      </Box>

      <Box gridColumn="span 6">
        <GardenTable
          dataSource={organicMatters}
          highlightItem={'COMPOST'}
          labelRatio={ui.organicMattersColumn}
          sourceItems={itemsOrganicMatter}
        />
      </Box>

      <Box gridColumn="span 6">
        <GardenChart dataSource={organicMatters} highlightItem="COMPOST" max={10} text="name" />
      </Box>
      <Box gridColumn="span 12">
        <Typography variant="h3">{ui.fuelTitle}</Typography>
      </Box>

      <Box gridColumn="span 6">
        <GardenTable dataSource={fuels} highlightItem="BIOFUEL" labelRatio={ui.fuelColumn} sourceItems={itemsFuels} />
      </Box>
      <Box gridColumn="span 6">
        <GardenChart dataSource={fuels} highlightItem="BIOFUEL" showLabel text="ratio" />
      </Box>
    </Box>
  );
};
