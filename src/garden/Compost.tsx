import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { FC, useMemo } from 'react';

import { Coin } from '../components/Coin';
import { Item } from '../components/Item';
import { useLanguage } from '../resources/lang/LanguageContext';
import type { ILanguageItems } from '../resources/lang/type';

import type { IDataSourceItem } from './types';

interface IMinItem {
  name: string;
  price: number;
  ratio: number;
}

const getMinItem = (source: Partial<Record<keyof ILanguageItems, IDataSourceItem>>): IMinItem => {
  return Object.keys(source)
    .map((name) => ({ name, ...(source[name as keyof ILanguageItems] as { price: number; ratio: number }) } as IMinItem))
    .reduce((min, current) => {
      if (min === undefined) {
        return current;
      }

      if (current.ratio < min.ratio) {
        return current;
      }

      return min;
    }, undefined as undefined | IMinItem) as IMinItem;
};

export const Compost: FC<{
  compost: { buy: number; sell: number };
  fuels: Partial<Record<keyof ILanguageItems, IDataSourceItem>>;
  organicMatters: Partial<Record<keyof ILanguageItems, IDataSourceItem>>;
}> = ({ compost, fuels, organicMatters }) => {
  const { ui } = useLanguage();

  const compostCraft = useMemo(() => {
    const minimalMatter = getMinItem(organicMatters);

    const minimalFuel = getMinItem(fuels);

    if (!minimalFuel || !minimalMatter) {
      return undefined;
    }

    const { sell } = compost;
    const cost = minimalFuel.ratio + minimalMatter.ratio;

    return { cost, fuel: minimalFuel, matter: minimalMatter, profit: sell - cost, sell };
  }, [compost, fuels, organicMatters]);

  if (!compostCraft) {
    return <></>;
  }

  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 650 }}>
        <TableHead>
          <TableRow>
            <TableCell></TableCell>
            <TableCell>{ui.matter}</TableCell>
            <TableCell>{ui.fuel}</TableCell>
            <TableCell align="right">{ui.itemPrice}</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell component="th" scope="row">
              Craft
            </TableCell>

            <TableCell>
              <Item>{compostCraft.matter.name}</Item>
            </TableCell>
            <TableCell>
              <Item>{compostCraft.fuel.name}</Item>
            </TableCell>
            <TableCell align="right">
              <Coin amount={compostCraft.cost} />
            </TableCell>
          </TableRow>
          <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell component="th" scope="row">
              Sell price
            </TableCell>
            <TableCell align="right" colSpan={3}>
              <Coin amount={compostCraft.sell} />
            </TableCell>
          </TableRow>
          <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell component="th" scope="row">
              Profits
            </TableCell>
            <TableCell align="right" colSpan={3}>
              <Coin amount={compostCraft.profit} />
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </TableContainer>
  );
};
